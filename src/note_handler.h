#ifndef NOTE_HANDLE_LIB
#define NOTE_HANDLE_LIB

#include <Arduino.h>
#include "magic_numbers.h"
/*
note_handler handles the notes received by midi_handler.
The note assignment logic is handled, and proper values are sent
to the DAC.
*/
class NoteHandler {
    public:
        void init();

        void handle_note_on(byte note);
        void handle_note_off(byte note);

        byte get_first_active_note() { return active_notes[FIRST_NOTE]; }
        void set_first_active_note(byte note) { this->active_notes[FIRST_NOTE] = note; }

        byte get_second_active_note() { return active_notes[SECOND_NOTE]; }
        void set_second_active_note(byte note) { this->active_notes[SECOND_NOTE] = note; }

        byte* get_active_notes() { return active_notes; }

        void push_note_to_queue(byte note);
        byte pop_note_from_queue();
    private:
        // Currently playing notes
        byte active_notes[2] = {0, 0};
        // Number of voices currently playing
        uint8_t playing_voices;
        // List of active notes, that are not currently playing
        byte note_queue[8] = {0};
        int8_t queue_position = 0;
        // duophonic: 0, monophonic: 1
        byte playing_mode = 0;
        // All notes, either 1: on, or 0: off
        uint16_t pressed_notes[4] = {0};
};

#endif // NOTE_HANDLE_LIB