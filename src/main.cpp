#include <Arduino.h>
#include "midi_handler.h"
#include "dac.h"
#include "magic_numbers.h"

MidiHandler MIDIin;
DAC MyAssDac;

void setup() {
  MIDIin.init();

  MyAssDac.init();
}

void loop() {
  MIDIin.poll();
}