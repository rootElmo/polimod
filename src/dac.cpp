#include "dac.h"
#include "mcp4728.h"
#include "Wire.h"
#include "magic_numbers.h"

mcp4728 MyDac(0);

int DAC::init() {
    
    MyDac.begin();
    MyDac.vdd(5000);

    pinMode(CS_DAC, OUTPUT);
    digitalWrite(CS_DAC, LOW);

    set_settings();

    return MyDac.getId();
}

void DAC::update_notes(byte notes[]) {
    send_note(FIRST_CHANNEL, notes[FIRST_NOTE]);
    send_note(SECOND_CHANNEL, notes[SECOND_NOTE]);
}

int DAC::send_note(byte channel, byte note) {
    // Only accept channels 0 & 1
    if (channel > 1) return ERROR_CODE_GENERAL;
    // DAC 0 & 1 voltage inversely proportional to MIDI pitch
    int write_value = MAX_DAC_OUT - ((int) (note * NOTE_INCREMENT));

    if (write_value > MAX_DAC_OUT) write_value = MAX_DAC_OUT;
    if (write_value < 0) write_value = 0;
    MyDac.voutWrite(channel, write_value);
    return MyDac.getVout(channel);
}

int DAC::send_vca_cc(byte value) {
    int write_value = MAX_DAC_OUT - ((int) (value * CC_INCREMENT));

    if (write_value > MAX_DAC_OUT) write_value = MAX_DAC_OUT;
    if (write_value < 0) write_value = 0;
    MyDac.voutWrite(THIRD_CHANNEL, write_value);
    return MyDac.getVout(THIRD_CHANNEL);
}

int DAC::send_vcf_velocity(byte value) {
    int write_value = CC_INCREMENT * value;
    if (write_value > MAX_DAC_OUT) write_value = MAX_DAC_OUT;
    if (write_value < 0) write_value = 0;
    MyDac.voutWrite(FOURTH_CHANNEL, write_value);
    return MyDac.getVout(FOURTH_CHANNEL);
}

int DAC::set_settings() {
    int ret_code;
    ret_code = MyDac.setVref(1,1,1,1);
    if (ret_code != 0) return ret_code;
                    
    ret_code = MyDac.setPowerDown(0, 0, 0, 0);

    if (ret_code != 0) return ret_code;

    ret_code = MyDac.setGain(1, 1, 1, 1);
    if (ret_code != 0) return ret_code;
    return 0;
}

int DAC::get_id() {
    return MyDac.getId();
}

int DAC::get_vref(byte chn) {
    return MyDac.getVref(chn);
}

int DAC::update_outputs() {
    return MyDac.update();
}