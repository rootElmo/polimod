#include <EEPROM.h>
#include "midi_handler.h"
#include "magic_numbers.h"
#include "elapsedMillis.h"
#include "note_handler.h"
#include "dac.h"

MIDI_CREATE_INSTANCE(HardwareSerial, Serial, MIDI);
elapsedMillis Midi_blink_timer_ms;
NoteHandler Note_handler;
DAC Dac;

void MidiHandler::init() {
    pinMode(LED_MIDI, OUTPUT);
    pinMode(LED_PROGR, OUTPUT);
    pinMode(PROGR_BTN, INPUT);
    this->midi_channel = get_midi_channel_eeprom();
    byte channel = this->midi_channel;
    programming_mode = false;

    if (channel == 255 || channel > 17 || channel == 0){
        MIDI.begin(MIDI_CHANNEL_OMNI);
        this->midi_channel = MIDI_CHANNEL_LISTEN_ALL;
    } else {
        MIDI.begin(MIDI_CHANNEL_OMNI);
    }

    Note_handler.init();

    digitalWrite(LED_MIDI, LOW); 
}

byte MidiHandler::get_midi_channel_eeprom() {
    return EEPROM.read(MIDI_CHANNEL);
}

void MidiHandler::set_midi_channel_eeprom(byte channel) {
    if (channel < 1 || channel > 16) {
        return;
    }
    set_midi_channel(channel);
    EEPROM.write(MIDI_CHANNEL, channel);
}

void MidiHandler::handle_midi_message(byte message) {
    switch (message) {
    case midi::NoteOn:
    case midi::NoteOff: {
        byte pitch = MIDI.getData1();
        byte velocity = MIDI.getData2();

        if (pitch > MAX_NOTES || pitch < 0) break;
        if (message == midi::NoteOff) velocity = 0;

        if (velocity == 0) {
            Note_handler.handle_note_off(pitch);
            Dac.update_notes(Note_handler.get_active_notes());
            break;
        } else {
            Note_handler.handle_note_on(pitch);
            // Set Velocity
            Dac.update_notes(Note_handler.get_active_notes());
            Dac.send_vcf_velocity(velocity);
            break;
        }
    }
    break;

    case midi::ControlChange: {
        byte controller = MIDI.getData1();
        byte value = MIDI.getData2();
        if (controller == midi::ChannelVolume || controller == midi::ModulationWheel) {
            Dac.send_vca_cc(value);
        }
        break;
    }
    break;
    }
}

void MidiHandler::poll() {
    programming_mode = digitalRead(PROGR_BTN);
    digitalWrite(LED_PROGR, programming_mode);
    
    if (MIDI.read()) {
        byte channel = MIDI.getChannel();

        if (channel != this->midi_channel && this->midi_channel != MIDI_CHANNEL_LISTEN_ALL && !programming_mode) {
            return;
        } else if (channel != this->midi_channel && !programming_mode) {
            return;
        } else if (channel != this->midi_channel && programming_mode) {
            this->midi_channel = channel;
            set_midi_channel_eeprom(this->midi_channel);
        }

        digitalWrite(LED_MIDI, HIGH);
        Midi_blink_timer_ms = 0;
        
        byte message = MIDI.getType();
        handle_midi_message(message);
    }

    if (Midi_blink_timer_ms > BLINK_T) digitalWrite(LED_MIDI, LOW);
}