#ifndef MIDI_HANDLE_LIB
#define MIDI_HANDLE_LIB

#include "../lib/arduino_midi_library/src/MIDI.h"
/*
midi_handler handles MIDI messages and MIDI settings.
midi_handler uses the note_handler to then process
the notes and note assignment logic.
*/
class MidiHandler {
    public:
        void init();
        void poll();

        void handle_midi_message(byte message);
        byte get_midi_channel_eeprom();
        void set_midi_channel_eeprom(byte channel);
        byte get_midi_channel() { return this->midi_channel; }
        void set_midi_channel(byte channel) { this->midi_channel = channel; }
    private:
        byte midi_channel;
        bool programming_mode;
};

#endif // MIDI_HANDLE_LIB
