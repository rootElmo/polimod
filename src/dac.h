#ifndef DAC_LIB
#define DAC_LIB
#include <Arduino.h>

class DAC {
    public:
        int init();
        int set_settings();
        void update_notes(byte notes[]);
        int send_note(byte channel, byte note);
        int send_vca_cc(byte value);
        int send_vcf_velocity(byte value);
        int get_id();
        int get_vref(byte chn);
        int update_outputs();
    private:
};

#endif // DAC_LIB