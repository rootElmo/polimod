#ifndef MAGIC_NUMS_LIB
#define MAGIC_NUMS_LIB

/*-----[PINS]-----*/
constexpr int LED_PROGR = 15;
constexpr int LED_MIDI = 16;
constexpr int GATE_OUT = 7;
constexpr int CS_DAC = 17;
constexpr int PROGR_BTN = 8;

/*-----[TIMERS]-----*/
constexpr int BLINK_T = 100;

/*-----[MIDI CONSTANTS]-----*/
constexpr int MAX_NOTES = 64;
constexpr int MAX_CC = 127;
constexpr int MAX_DAC_OUT = 4000;
constexpr float NOTE_INCREMENT = 62.5f;
constexpr float CC_INCREMENT = 31.25f;
constexpr int MIDI_CHANNEL_LISTEN_ALL = 255;

/*-----[NOTE CONSTANTS]-----*/
constexpr int FIRST_NOTE = 0;
constexpr int SECOND_NOTE = 1;
constexpr int QUEUE_MAX_INDEX = 7;
constexpr int QUEUE_MIN_INDEX = 0;

/*-----[DAC CONSTANTS]-----*/
constexpr int FIRST_CHANNEL = 0;
constexpr int SECOND_CHANNEL = 1;
constexpr int THIRD_CHANNEL = 2;
constexpr int FOURTH_CHANNEL = 3;
constexpr int ERROR_CODE_GENERAL = 500;

/*-----[EEPROM ADDRESSES]-----*/
constexpr int MIDI_CHANNEL = 0;

#endif // MAGIC_NUMS_LIB