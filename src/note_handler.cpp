#include "note_handler.h"

void NoteHandler::init() {
    playing_voices = 0;
    playing_mode = 0;

    pinMode(GATE_OUT, OUTPUT);
    digitalWrite(GATE_OUT, LOW);
}

void NoteHandler::handle_note_on(byte note) {
    byte first_note = get_first_active_note();
    byte second_note = get_second_active_note();

    switch (playing_voices) {
    case 0:
        set_first_active_note(note);
        set_second_active_note(note);

        digitalWrite(GATE_OUT, HIGH);
        playing_voices++;
        break;
    case 1:
        /*
        If incoming note is higher than any playing note,
        first voice gets the new note,
        second voice gets the old first note.

        If incoming note is lower than playing note,
        second voice gets the new note
        */
        if (note > first_note) {
            set_second_active_note(first_note);
            set_first_active_note(note);
        } else {
            set_second_active_note(note);
        }
        playing_voices++;
        break;
    // When over 2 notes are active
    default:
        if (note > first_note || note > second_note) {
            set_first_active_note(note);
            push_note_to_queue(first_note);
        } else {
            set_second_active_note(note);
            push_note_to_queue(second_note);
        }
        playing_voices++;
        break;
    }
}

void NoteHandler::handle_note_off(byte note) {
    byte first_note = get_first_active_note();
    byte second_note = get_second_active_note();

    switch (playing_voices) {
    case 0:
        return;
    case 1:
        if (note != first_note) return;

        playing_voices--;
        digitalWrite(GATE_OUT, LOW);
        break;
    case 2:
        if (note != first_note && note != second_note) {
            return;
        } else if (note == first_note)  {
            set_first_active_note(second_note);
        } else if (note == second_note) {
            set_second_active_note(first_note);
        } else {
            // Should not get here :)
            return;
        }
        playing_voices--;
        break;
    // When there are notes in queue (+2 notes active)
    default:
        byte note_from_queue = pop_note_from_queue();

        if (note_from_queue > first_note) {
            set_first_active_note(note_from_queue);
        } else if (note_from_queue > second_note
                && note_from_queue < first_note) {
            set_first_active_note(note_from_queue);
        } else {
            set_second_active_note(note_from_queue);
        }
        playing_voices--;
        break;
    }
}

void NoteHandler::push_note_to_queue(byte note) {
    note_queue[queue_position] = note;
    queue_position++;
    if (queue_position > QUEUE_MAX_INDEX) queue_position = QUEUE_MIN_INDEX;
}

byte NoteHandler::pop_note_from_queue() {
    queue_position--;
    if (queue_position < QUEUE_MIN_INDEX) queue_position = QUEUE_MAX_INDEX;
    return note_queue[queue_position];
}